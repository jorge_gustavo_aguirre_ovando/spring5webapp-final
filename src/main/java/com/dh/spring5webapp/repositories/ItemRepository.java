/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.dh.spring5webapp.model.Item;

public interface ItemRepository extends CrudRepository<Item, Long> {
    Optional<Item> findById(Long id);
}
