/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.dh.spring5webapp.command.ItemCommand;
import com.dh.spring5webapp.repositories.ItemRepository;

@Controller
@Path("/items")
@Produces("application/json")
public class ItemController {
    private ItemRepository itemRepository;

    @Autowired
    private ItemRepository itemService;

    public ItemController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    /*@RequestMapping("/items")
    public String getItems(Model model) {
        model.addAttribute("items", itemRepository.findAll());
        return "items";
    }

    @RequestMapping("/items/{id}")
    public String getItem(Model model, @PathVariable String id) {
        model.addAttribute("item", itemService.findById(Long.valueOf(id)).get());
        return "item";
    }*/

    @OPTIONS
    @CrossOrigin
    public Response prefligth() {
        return Response.ok().header("Access-Control-Allow-Origin", "*")
              .header("Access-Control-Allow-Credentials", "true")
              .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").header("Access-Control-Allow-Headers",
                    "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
              .allow("OPTIONS").build();
    }

    @GET
    @CrossOrigin
    public Response getItems() {
        List<ItemCommand> items = new ArrayList<>();
        itemRepository.findAll().iterator().forEachRemaining(item -> {
            ItemCommand itemCommand = new ItemCommand(item);
            items.add(itemCommand);
        });
        return Response.ok(items).header("Access-Control-Allow-Origin", "*")
              .header("Access-Control-Allow-Credentials", "true")
              .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").header("Access-Control-Allow-Headers",
                    "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")

              .build();
    }


}
