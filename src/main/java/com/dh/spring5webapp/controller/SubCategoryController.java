/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.repositories.SubCategoryRepository;

//@Controller
public class SubCategoryController {
    private SubCategoryRepository subCategoryRepository;

    public SubCategoryController(SubCategoryRepository subCategoryRepository) {
        this.subCategoryRepository = subCategoryRepository;
    }

    @RequestMapping("/subCategorys")
    public String getSubCategorys(Model model) {
        model.addAttribute("subCategorys", subCategoryRepository.findAll());
        return "subCategorys";
    }

}
