/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Controller;

import com.dh.spring5webapp.command.EmployeeCommand;
import com.dh.spring5webapp.model.Employee;
import com.dh.spring5webapp.repositories.EmployeeRepository;

@Path("/employees")
@Produces("application/json")
@Controller
public class EmployeeController {
    private EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GET
    public Response getEmployees() {
        List<EmployeeCommand> employeeList = new ArrayList<>();
        employeeRepository.findAll().iterator().forEachRemaining(employee -> {

            employeeList.add(new EmployeeCommand(employee));
        });
        return Response.ok(employeeList).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public EmployeeCommand getEmployeeById(@PathParam("id") long id) {
        Employee employee = employeeRepository.findOne(id);
        return new EmployeeCommand(employee);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public EmployeeCommand addEmployee(EmployeeCommand employeeCommand) {
        Employee employee = employeeRepository.save(employeeCommand.toEmployee());
        return new EmployeeCommand(employee);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public EmployeeCommand updateEmployee(EmployeeCommand employeeCommand) {
        Employee employee = employeeRepository.save(employeeCommand.toEmployee());
        return new EmployeeCommand(employee);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteEmployee(@PathParam("id") long id) {
        employeeRepository.delete(id);

    }
}