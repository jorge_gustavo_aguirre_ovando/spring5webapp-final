/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.spring5webapp.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.repositories.ContractRepository;

//@Controller
public class ContractController {
    private ContractRepository contractRepository;

    public ContractController(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    @RequestMapping("/contracts")
    public String getContracts(Model model) {
        model.addAttribute("contracts", contractRepository.findAll());
        return "contracts";
    }

}
